package com.codeninja.book.basics


/**
 * The functions checks if the given string is a palindrome or not.
 */
fun isPalindrome(str: String): Boolean {
    throw NotImplementedError()
}

/**
 * The function returns the position of each element of the list `items` in the list `list`.
 * If any item of items doesn't exist in list, the corresponding position is set to be -1.
 * If an item exists at multiple positions in the list then return the position of first occurrence.
 *
 * Example:
 * list: ["good", "day", "how", "do", "good"]
 * items: ["day", "done", "good"]
 * returs: [1, -1, 0]
 */
fun positionInList(list: List<String>, items: List<String>): List<Int> {
    throw NotImplementedError()
}

/**
 * Checks if String a can be converted into String b by rearranging the alphabets of a
 *
 * Examples:
 * a -> "aabccded" b->"decdcbaa"  = True
 * a -> "abb" b->"abc"  = False
 */
fun isConvertibleWithRearrangements(a: String, b: String): Boolean {
    throw NotImplementedError()
}

private fun String.charCount(): Map<Char, Int> {
    throw NotImplementedError()
}

/**
 * Checks if String a can be converted into String b by rearranging the alphabets of a and possibly
 * ignoring some characters of a.
 *
 * Examples:
 * a -> "aabccded" b->"decdcbaa"  = True
 * a -> "abb" b->"abc"  = False
 * a -> "abbdec" b->"abc"  = True           //ignored bde
 */
fun isConvertibleWithRearrangementsAndDeletion(a: String, b: String): Boolean {
    throw NotImplementedError()
}


/**
 * Returns true if list a contains all strings in list b
 */
fun contains(a: List<String>, b: List<String>): Boolean {
    throw NotImplementedError()
}

/**
 * Returns the sorted String
 *
 * Example
 * Input: "gooday"
 * Returns: "adgooy"
 */
fun sortTheString(a: String): String {
    throw NotImplementedError()
}