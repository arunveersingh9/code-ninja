package com.codeninja.book.basics

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource
import kotlin.random.Random

internal class StringsTest {

    @ParameterizedTest
    @ValueSource(ints = [2, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000])
    fun isPalindromTest(size: Int) {
        generateRandomString(size).makeNonPalindrom()
                .apply { assertEquals(isPalindrome(this), false) }

        generateRandomString(size / 2).apply {
            assertEquals(isPalindrome(this + this.reversed()), true)
        }
    }

    @ParameterizedTest
    @ValueSource(ints = [2, 10, 100, 1000, 10000, 100000, 1000000, 10000000])
    //todo: Test is not checked
    fun positionInListTest(size: Int) {
        val list = generateListOfSize(size)
        var items = list.shuffled().subList(0, Random.nextInt(list.size)) + generateListOfSize(Random.nextInt(size))
        items = items.shuffled()

        val listSet = list.toSet()
        val returned = positionInList(list, items)
        (0 until items.size)
                .map {
                    assertEquals(returned[it] != -1, listSet.contains(items[it]))
                    assertTrue(returned[it] == -1 || list[returned[it]] == items[it])
                }
    }

    @ParameterizedTest
    @ValueSource(ints = [2, 10, 100, 1000, 10000, 100000, 1000000, 10000000])
    fun isConvertibleWithRearrangementsTest(size: Int) {
        val str = generateRandomString(size)
        val randomized = str.shuffled()
        assertTrue(isConvertibleWithRearrangements(str, randomized))
        assertFalse(isConvertibleWithRearrangements(str, randomized + "x"))
    }

    @ParameterizedTest
    @ValueSource(ints = [2, 10, 100, 1000, 10000, 100000, 1000000, 10000000])
    fun isConvertibleWithRearrangementsAndDeletionTest(size: Int) {
        val str = generateRandomString(size)
        val randomized = str.shuffled()
        assertTrue(isConvertibleWithRearrangementsAndDeletion(str, randomized))
        assertTrue(isConvertibleWithRearrangementsAndDeletion(str + generateRandomString(size), randomized))
        assertFalse(isConvertibleWithRearrangementsAndDeletion(str.replace(randomized.first(), ' '), randomized))
    }

    @ParameterizedTest
    @ValueSource(ints = [2, 10, 100, 1000, 10000, 100000, 1000000])
    fun containsTest(size: Int) {
        val listB = generateListOfSize(size)
        val listAGood = listB.run { this + generateListOfSize(size) }.shuffled()
        val listABad = listAGood.filter { it != listB.first() }
        assertTrue(contains(listAGood, listB))
        assertFalse(contains(listABad, listB))
    }

    @ParameterizedTest
    @ValueSource(ints = [2, 10, 100, 1000, 10000, 100000, 1000000, 10000000])
    fun sortTheStringTest(size: Int) {
        val str = generateRandomString(size)
        sortTheString(str).apply {
            (1 until this.length).map {
                assertTrue(this[it] >= this[it - 1])
            }
            this.charCount() == str.charCount()
        }
    }

    private fun generateListOfSize(size: Int): List<String> = (1..size).map { RandomStringUtils.randomAlphanumeric(Random.nextInt(20)) }

    private fun generateRandomString(size: Int): String = RandomStringUtils.randomAlphanumeric(size)

    private fun String.makeNonPalindrom(): String = takeIf { first() != last() } ?: last().inc() + this

    private fun String.shuffled() = this.toList().shuffled().joinToString("")

    private fun String.charCount(): Map<Char, Int> {
        val counter = HashMap<Char, Int>()
        this.toCharArray()
                .map { counter.put(it, counter.getOrDefault(it, 0) + 1) }
        return counter
    }
}
